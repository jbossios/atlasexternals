// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
// thread17_test: testing check_attrib_consistency

#pragma ATLAS check_thread_safety

void f1 [[gnu::not_const_thread_safe]] ();
void f1 [[gnu::not_reentrant]] () {}

void f2 ();
void f2 [[gnu::not_thread_safe]] () {}
