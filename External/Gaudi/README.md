Gaudi
=====

This package builds [Gaudi](https://gaudi-framework.readthedocs.io), the
framework on which all of the ATLAS "offline" code is built on top of.
