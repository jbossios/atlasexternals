# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  IPADDRESS_PYTHON_PATH
#
# Can be steered by IPADDRESS_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME ipaddress
   PYTHON_NAMES ipaddress/__init__.py ipaddress.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( ipaddress DEFAULT_MSG
   _IPADDRESS_PYTHON_PATH )

# Set up the RPM dependency.
lcg_need_rpm( ipaddress )
